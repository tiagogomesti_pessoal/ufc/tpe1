#include "ArvBin.h"

int cont = 0;
/***************************************************************/

Arv* inicializa(void)
{
    return NULL;
}

/***************************************************************/

Arv* cria(int c, Arv* sae, Arv* sad)
{
    Arv* p=(Arv*)malloc(sizeof(Arv));
    p->info = c;
    p->esq = sae;
    p->dir = sad;
    return p;
}

/***************************************************************/

int vazia(Arv* a)
{
    return a==NULL;
}

/***************************************************************/

void imprime (Arv* a)
{
    if (!vazia(a))
    {
        printf("%d ", a->info); /* mostra raiz */
        imprime(a->esq); /* mostra sae */
        imprime(a->dir); /* mostra sad */
    }
}

/***************************************************************/

Arv* libera (Arv* a)
{
    if (!vazia(a))
    {
        libera(a->esq); /* libera sae */
        libera(a->dir); /* libera sad */
        free(a); /* libera raiz */
    }
    return NULL;
}

/***************************************************************/

int busca (Arv* a, int c)
{
    if (vazia(a))
        return 0; /* �rvore vazia: n�o encontrou */
    else
        return a->info==c || busca(a->esq,c) || busca(a->dir,c);
}

/***************************************************************/

int altura ( Arv* a)
{
	int i = 0;
	int j = 0;
	int h;
	if (!vazia(a))
	{
		if (!vazia(a->esq))
			i = altura(a->esq) + 1;
		if (!vazia(a->dir))
			j = altura(a->dir) + 1;

		if ( i>j )
			h = i;
		else h = j;
	}
	return h;
}

/***************************************************************/

void imprimeFormat(Arv* a)
{
	int i;
	int contAux;

	if(!vazia(a))
	{		
		for(i=0;i<cont;i++)
			printf("--");
		printf("%d\n",a->info);
		
		if(vazia(a->esq))
		{
			for (i=0; i<(cont+1); i++)
				printf("--");
			printf("#\n");
		}
		if(vazia(a->dir))
		{
			for (i=0; i<(cont+1); i++)
				printf("--");
			printf("#\n");
		}
					
		contAux = cont++;
		imprimeFormat(a->esq);
		cont = ++contAux;
		imprimeFormat(a->dir);
		
	}
}

/***************************************************************/













