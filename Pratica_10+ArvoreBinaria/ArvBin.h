#ifndef ARVBIN_H_INCLUDED
#define ARVBIN_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>


struct arv {
int info;
struct arv* esq;
struct arv* dir;
};
typedef struct arv Arv;


Arv* inicializa(void);
Arv* cria(int c, Arv* sae, Arv* sad);
int vazia(Arv* a);
void imprime (Arv* a);
Arv* libera (Arv* a);
int busca (Arv* a, int c);
int altura ( Arv* a);
void imprimeFormat(Arv* a);
#endif // ARVBIN_H_INCLUDED
