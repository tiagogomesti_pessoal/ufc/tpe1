#ifndef PILHA_H_INCLUDED
#define PILHA_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>

struct no {
float info;
struct no* prox;
};
typedef struct no No;

struct pilha {
No* prim;
};
typedef struct pilha Pilha;

Pilha* cria (void);
No* ins_ini (No* l, float v);
No* ret_ini (No* l);
void push (Pilha* p, float v);
float pop (Pilha* p);
int vazia (Pilha* p);
void libera (Pilha* p);
void calcula (Pilha* p);

#endif // PILHA_H_INCLUDED
