#include "pilha.h"

Pilha* cria (void)
{
	Pilha* p = (Pilha*) malloc(sizeof(Pilha));
	p->prim = NULL;
	return p;
}

/* fun��o auxiliar: insere no in�cio */
No* ins_ini (No* l, float v)
{
	No* p = (No*) malloc(sizeof(No));
	p->info = v;
	p->prox = l;
	return p;
}

/* fun��o auxiliar: retira do in�cio */
No* ret_ini (No* l)
{
	No* p = l->prox;
	free(l);
	return p;
}

void push (Pilha* p, float v)
{
	p->prim = ins_ini(p->prim,v);
}

float pop (Pilha* p)
{
	float v;
	if (vazia(p))
	{
		printf("Pilha vazia.\n");
		exit(1); /* aborta programa */
	}
	v = p->prim->info;
	p->prim = ret_ini(p->prim);
	return v;
}

int vazia (Pilha* p)
{
	return (p->prim==NULL);
}

void libera (Pilha* p)
{
	No* q = p->prim;
	No* t = NULL;
	while (q!=NULL)
	{
		t = q->prox;
		free(q);
		q = t;
	}
	free(p);
}

void calcula (Pilha* p)
{
	char str[10];


    do
    {
        while ( 1 )
        {
            //if ( !(strcmp(str,"=")) ) break;
            printf ("---> "); fflush(stdin); gets(str);
            if ( !(strcmp(str,"=")) ) break;

            if (!(strcmp(str,"+")))
            {
                push (p, pop(p) + pop(p));
                continue;
            }
            if (!(strcmp(str,"-")))
            {
                push ( p, (-1)*(pop(p) - pop(p)) );
                continue;
            }
            if (!(strcmp(str,"*")))
            {
                push (p, pop(p) * pop(p));
                continue;
            }
            if (!(strcmp(str,"/")))
            {
                push (p, (1.0/pop(p)) * pop(p) );
                continue;
            }

            push(p, atof(str));

        }
        printf ("Resultado---> %f\n",pop(p));
        printf ("Quer fazer outra operacao?(s/n) "); fflush(stdin); gets(str); printf ("\n\n");
    } while ( !(strcmp(str,"s")) || !(strcmp(str,"S")) );

}














