#include <stdio.h>
#include <stdlib.h>
#define TAXA_A  5
#define TAXA_B 10
#define TAXA_C 15
#define TAXA_D 20
#define TAXA_E 25

typedef struct record{
  int id;
  int trash;
  double value;
} Record;

Record *ReadBinFile(int *n);
double Total(Record *r, int n);
char Range(double value);
int Percent(double value);
void Report(Record *r, int n);
void freeRecord(Record *r, int n);
