#include "record.h"


Record *ReadBinFile(int *n)    // retorna um vetor de Record.
{
    FILE* fp;
    Record *r;
    int i;


    if ( ( fp = fopen("dados.bin","rb") ) == NULL ) // abertura do arquivo
    {
        printf ("Erro na abertura do arquivo...");
        exit(1);
    }

    fread(n, sizeof(int), 1, fp); // pegando do arquivo o valor de n

    if ( ( r = (Record*) malloc( (*n)*sizeof(Record) ) ) == NULL ) // aloca��o de r
    {
        printf ("Erro na alocacao dinamica..");
        exit(1);
    }

    for (i=0; i<(*n); i++)
    {
        fread ( (r+i), sizeof(Record), 1, fp );
    }

    while ( fclose( fp ) != 0 )
    {
        printf ("Erro ao fechar o arquivo...");
    }

    return r;
}

double Total(Record *r, int n) // retorna o valor gasto em salario
{
    double total = 0;
    int i;

    for (i=0; i<n; i++)
    {
        total += (r+i)->value;
    }

    return total;
}

void Report(Record *r, int n)  // impress�es e calculo dos novos salarios
{
    int i;
    int perc;
    double aux;
    double salarioAnterior;

    printf ("\n\n");
    for (i=0; i<n; i++)
    {
       perc = Percent((r+i)->value);
       aux = (double) perc/100.0;
       salarioAnterior = (r+i)->value;
       (r+i)->value += aux*(r+i)->value;

	   printf ("id: %d faixa: %c salario ant: R$ %.2lf porc: %d%% salario atual: R$ %7.2lf\n\n",
            (r+i)->id,
            Range((r+i)->value),
            salarioAnterior,
            Percent( (r+i)->value ),
			(r+i)->value
			);
    }
}

char Range(double value) // retorna o caracter referente a faixa de aumento
{
  if ( value >= 1200.0 )
    return 'A';
  if ( value >=  900.0 )
    return 'B';
  if ( value >=  600.0 )
    return 'C';
  if ( value >=  300.0 )
    return 'D';
    else
	  return 'E';
}

int Percent(double value) // retorna a porcentagem de cada faixa
{
  if ( value >= 1200.0 )
    return TAXA_A;
  if ( value >=  900.0 )
    return TAXA_B;
  if ( value >=  600.0 )
    return TAXA_C;
  if ( value >=  300.0 )
    return TAXA_D;
    else
	  return TAXA_E;
}

void freeRecord(Record *r, int n)
{
    int i;

    for (i=0; i<n; i++)
      free( r+i );
}








