#include <stdio.h>
#include <stdlib.h>
#include "record.h"

int main(void) {
  int n;
  Record *r;

  r = ReadBinFile(&n);
  printf("Gasto Total Antes: %.2lf\n", Total(r, n));
  Report(r, n);
  printf("Gasto Total Depois: %.2lf\n", Total(r, n));
  //free(r);
  freeRecord(r,n);

  system("PAUSE");
  return 0;
}
