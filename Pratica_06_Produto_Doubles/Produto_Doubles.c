#include <stdio.h>
#include <stdlib.h>

typedef struct {
  double **address;
  int rows;
  int coluns;
} DoubleArray;

void readDoubleArray(DoubleArray *x){
  int i,j;

  printf ("Digite o numero de linhas: "); scanf("%d",&x->rows);
  printf ("Digite o numero de colunas: "); scanf("%d",&x->coluns);

  x->address = (double **) malloc ( x->rows*sizeof(double *) ); // aloca��o das linhas
  if ( !x->address ) { printf ("Erro ao alocar as linhas da matriz, sem espaco na memoria suficiente...\n"); system("PAUSE"); } // teste de aloca��o

  for (i=0; i<x->rows; i++) {
      *(x->address+i) = (double *) malloc ( x->coluns * sizeof(double) ); // aloca��o das colunas
      if ( !(*(x->address+i)) ) {printf ("Erro ao alocar as colunas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE");} // teste de aloca��o
  }
  printf ("Entrada dos valores da matriz.\n");


  for (i=0; i<x->rows; i++)
    for (j=0; j<x->coluns; j++) {
      printf ("Posicao %dx%d: ", i+1,j+1); scanf ("%lf",*(x->address+i) + j ); } printf ("\n"); // entrada dos valores de cada elemento
}

DoubleArray multiplyDoubleArray(DoubleArray *x, DoubleArray *y) {

  DoubleArray c;
  int i,j,k;


  if (x->coluns != y->rows) { // teste para saber se o produto pode ser feito
    printf ("O numero de colunas da primeira tem que ser igual a numero de linhas da segunda..."); c.address = NULL; return c;}
  c.rows = x->rows ;  c.coluns = y->coluns;

  c.address = (double **) malloc ( c.rows * sizeof(double *) ); // aloca��o das linhas
  if ( !c.address ) { printf ("Erro ao alocar as linhas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE");
  c.address=NULL; return c;} // teste de aloca��o,se n�o alocado retorna c com c.address = NULL, ver main

  for (i=0; i<c.rows; i++) { // aloca��o das colunas
    *(c.address+i) = (double *) malloc ( c.coluns * sizeof(double) );
    if ( !( *(c.address+i) ) ) { printf ("Erro ao alocar as colunas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE");
    c.address=NULL; return c; } } // teste de aloca��o,se n�o alocado retorna c com c.address = NULL, ver main

  for (i=0; i<c.rows; i++) // zerando os elementos da matriz resultante
    for (j=0; j<c.coluns; j++)  *( *(c.address+i) + j)  = 0.0;

  for (i=0; i<c.rows; i++)
    for (j=0; j<c.coluns; j++)
      for ( k=0; k<y->rows; k++) // calculo dos elementos da matriz resultante c[i][j] = sum(k=(1:n)) x->address[i][k] x y->address[k][j]
                  *( *(c.address+i) + j) += (*( *(x->address+i) + k))*(*( *(y->address+k) + j));

  return c;
}

void showDoubleArray(DoubleArray *x) {
  int i,j;

  for (i=0; i<x->rows; i++) {
    for (j=0; j<x->coluns; j++) { // impress�o na tela dos elementos da matriz
      printf ("%7.2lf  ",*(*(x->address + i) + j) ); } printf ("\n");  } printf ("\n\n");
}

void freeDoubleArray(DoubleArray *x){

  int i;

  for (i=0; i<x->rows; i++) free ( *(x->address+i) ); // libera��o das linhas
  free (x->address); // libera��o do ponteiro princinpal
}


int main(void) {
  DoubleArray a, b, c;

  readDoubleArray(&a);
  readDoubleArray(&b);
  if(a.address && b.address) { /* aloca��es e leituras com sucesso */
    c = multiplyDoubleArray(&a, &b);
    if(c.address) { /* aloca��o e multiplica��o com sucesso */
      showDoubleArray(&a);
      showDoubleArray(&b);
      showDoubleArray(&c);
      freeDoubleArray(&a);
      freeDoubleArray(&b);
      freeDoubleArray(&c);
    }
  }

  return 0;
}
