#include <stdio.h>
#include <stdlib.h>

typedef float * vetor;
typedef vetor * matriz;

int main (void)
{
    // declarações das variaveis
    matriz a;    int lin_a, col_a;
    matriz b;    int lin_b, col_b;
    matriz c;    int lin_c, col_c;
    int i,j,k;

    // entrada das dimenções das matrizes
    printf ("Digite a dimensao da primeira matriz.\n");
    printf ("Linhas: "); scanf("%d",&lin_a);
    printf ("Colunas: "); scanf("%d",&col_a); printf ("\n");

    printf ("Digite a dimensao da segunda matriz.\n");
    printf ("Linhas: "); scanf("%d",&lin_b);
    printf ("Colunas: "); scanf("%d",&col_b); printf ("\n");

    // verificação para saber se a multiplicação pode ser feita
    if ( col_a != lin_b ) { printf ("Erro...  O numero de colunas da primeira deve ser igual ao numero de linhas da\nsegunda...\n");
    system("PAUSE"); exit(0); }
    lin_c = lin_a; col_c = col_b;

    // alocação da primeira matriz
    a = (matriz) malloc ( (lin_a)*sizeof(vetor) ); // linhas
    if ( !a ) { printf ("Erro ao alocar as linhas da matriz, sem espaco na memoria suficiente...\n"); system("PAUSE"); exit(0); }

    for (i=0; i<lin_a; i++) { // colunas
        *(a+i) = (vetor) malloc ( (col_a) * sizeof(float) );
        if ( !(*(a+i)) ) {printf ("Erro ao alocar as colunas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE"); exit(0);}
    }

    // alocação da segunda matriz
    b = (matriz) malloc ( (lin_b)*sizeof(vetor) ); // linhas
    if ( !b ) { printf ("Erro ao alocar as linhas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE"); exit(0); }

    for (i=0; i<lin_b; i++) { // colunas
        *(b+i) = (vetor) malloc ( (col_b) * sizeof(float) );
        if ( !( *(b+i) ) ) { printf ("Erro ao alocar as colunas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE"); exit(0);}
    }

    // alocação da matriz resultante
    c = (matriz) malloc ( (lin_c)*sizeof(vetor) ); // linhas
    if ( !c ) { printf ("Erro ao alocar as linhas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE"); exit(0); }

    for (i=0; i<lin_c; i++) { // colunas
        *(c+i) = (vetor) malloc ( (col_c) * sizeof(float) );
        if ( !( *(c+i) ) ) { printf ("Erro ao alocar as colunas da matriz, sem espaco na memoria suficiente...\n");system("PAUSE"); exit(0);}
    }

    // entrada dos valores da primeira matriz
    printf ("Entrada dos valores da primeira matriz.\n");
    for (i=0; i<lin_a; i++)
        for (j=0; j<col_a; j++) {
            printf ("Posicao %dx%d: ", i+1,j+1); scanf ("%f",*(a+i) + j ); } printf ("\n");

    // entrada dos valores da segunda matriz
    printf ("Entrada dos valores da segunda matriz.\n");
    for (i=0; i<lin_b; i++)
        for (j=0; j<col_b; j++) {
            printf ("Posicao %dx%d: ", i+1,j+1); scanf ("%f",*(b+i) + j ); } printf ("\n");

    // zerando os elementos da matriz resultante
    for (i=0; i<lin_c; i++)
        for (j=0; j<col_c; j++)  *( *(c+i) + j)  = 0.0;

     // calculo dos elementos da matriz resultante c[i][j] = sum(k=(1:n)) a[i][k]xb[k][j]
     for (i=0; i<lin_c; i++)
        for (j=0; j<col_c; j++)
            for ( k=0; k<lin_b; k++)
                  *( *(c+i) + j) += (*( *(a+i) + k))*(*( *(b+k) + j));

    // apresentação na tela da matriz resultante
    printf ("Matriz resultante.\n\n");

    for (i=0; i<lin_c; i++) {
       for (j=0; j<col_c; j++) {
           printf ("%8.2f  ",*(*(c + i) + j) ); } printf ("\n");  } printf ("\n");



    for (i=0; i<lin_a; i++) free ( *(a+i) );
    for (i=0; i<lin_b; i++) free ( *(b+i) );
    for (i=0; i<lin_c; i++) free ( *(c+i) );

    free (a);
    free (b);
    free (c);

system("PAUSE");
return 1;
}




















