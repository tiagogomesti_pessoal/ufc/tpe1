#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED
/*
3. Considere a seguinte declara��o:
struct no
{
int codigo;
char nome[30];
float preco;
struct no *prox;
}
Crie um programa completo com fun��es para:
*a) criar uma lista vazia;
*b) verificar se a lista est� vazia ou n�o (retornar 1 se estiver vazia e 0 se tiver elementos);
*c) a inser��o de n�s na lista. Todo novo n� deve ser inserido sempre no final da lista;
*d) a exclus�o de um determinado n� da lista, conforme o c�digo digitado pelo usu�rio;
*e) listar todos os n�meros inseridos na lista;
f) reajustar o pre�o de todos os produtos conforme o percentual informado pelo usu�rio.*/
#endif // LISTA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

struct no{
int codigo;
char nome[30];
float preco;
struct no* prox;
};
typedef struct no No;

No* inicializa(void); // retorna uma lista vazia
No* insere (No* n, int codigo, char* nome, float preco); // insere no final
void imprime (No* n); // percorre a lista imprimindo cada elemento
int vazia (No* n); // retorna 1 se a lista tiver vazia
No* retira (No* n, int codigo);
void libera(No* n);
void reajustarPreco(No* n,float reajuste);
