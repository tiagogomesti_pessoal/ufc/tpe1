#include "lista.h"
/*
No* inicializa(void); // retorna uma lista vazia
No* insere (No* n); // insere no final
void imprime (No* n); // percorre a lista imprimindo cada elemento
int vazia (No* n); // retorna 1 se a lista tiver vazia
No* retira (No* n, int codigo);
void libera(No* n);
*/
int main(void)
{
    No* n;

    printf ("INSERSOES...\n");
    n = inicializa();
    n = insere(n,10,"coca-cola",1.50);
    n = insere(n,11,"arroz",2.00);
    n = insere(n,12,"feijao",2.20);
    n = insere(n,13,"sabao",1.40);
    n = insere(n,14,"detergente",1.10);
    n = insere(n,15,"cuzcuz",0.70);
    n = insere(n,16,"queijo",10.80);
    n = insere(n,17,"pao",5.50);
    imprime(n);
    printf ("/****************************************************************/\n");
    printf ("RETIRADAS...\n");
    n = retira(n, 12);
    imprime(n);
    printf ("/****************************************************************/\n");
    n = retira(n, 13);
    imprime(n);
    printf ("/****************************************************************/\n");
    printf ("INSERSOES...\n");
    n = insere(n,18,"ovo",0.20);
    n = insere(n,19,"farinha",1.20);
    n = insere(n,20,"bolacha",2.20);
    n = insere(n,21,"lampada",1.20);
    n = insere(n,22,"leite",2.00);
    imprime(n);
    printf ("/****************************************************************/\n");
    printf ("REAJUSTES...\n");
    reajustarPreco(n,15.0);
    imprime(n);
    printf ("/****************************************************************/\n");
    libera(n);

return 1;
}
